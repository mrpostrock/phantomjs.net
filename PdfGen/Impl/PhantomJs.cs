﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using PdfGen.Enums;

namespace PdfGen.Impl
{
    public class PhantomJs
    {
        private readonly Guid _name;

        private SourceType Type { get; set; }

        private string Basepath { get; }

        private string _remoteResource;

        public PhantomJs()
        {
            Basepath = Path.Combine(Path.GetTempPath(), "phantom");
            _name = Guid.NewGuid();

            CheckWorkFolder();
        }

        private void CheckWorkFolder()
        {
            if (!Directory.Exists(Basepath))
                Directory.CreateDirectory(Basepath);
        }

        public byte[] GeneratePdf(Uri remoteResource)
        {
            Type = SourceType.Remote;
            _remoteResource = remoteResource.ToString();

            ExctractConfig();
            ExtractPhantom();

            Generate();
            var result = GetResult();

            CleanUp();

            return result;
        }

        public byte[] GeneratePdf(string htmlMarkup)
        {
            Type = SourceType.Local;

            SaveLocalMarkUp(htmlMarkup);
            ExctractConfig();
            ExtractPhantom();

            Generate();

            var result = GetResult();
            CleanUp();

            return result;
        }

        private void SaveLocalMarkUp(string htmlMarkup)
        {
            using (var file = new FileStream($@"{Basepath}\source_{_name}.html", FileMode.Create, FileAccess.Write))
            {
                var bytes = Encoding.UTF8.GetBytes(htmlMarkup);

                file.Write(bytes, 0, bytes.Length);
            }
        }

        private void ExtractPhantom()
        {
            var executable = (byte[]) PhantomRes.ResourceManager.GetObject("phantom");

            if (executable == null)
                return;

            using (var fsDst = new FileStream($@"{Basepath}\{_name}.exe", FileMode.CreateNew, FileAccess.Write))
            {
                fsDst.Write(executable, 0, executable.Length);
            }
        }

        private void ExctractConfig()
        {
            var configFile = Type == SourceType.Remote ? PhantomRes.config : PhantomRes.config_local;

            if (Type == SourceType.Remote && string.IsNullOrEmpty(_remoteResource))
                configFile = configFile.Replace("*|URL|*", _remoteResource);

            if (Type == SourceType.Local)
                configFile = configFile.Replace("*|SOURCE|*", $@"{Basepath.Replace('\\', '/')}/source_{_name}.html");

            configFile = configFile.Replace("*|OUTPUT|*", $@"{Basepath.Replace('\\', '/')}/{_name}.pdf");

            using (var confingDst = new FileStream($@"{Basepath}\config_{_name}.js", FileMode.CreateNew, FileAccess.Write))
            {
                var bytes = Encoding.UTF8.GetBytes(configFile);

                confingDst.Write(bytes, 0, bytes.Length);
            }
        }

        private void Generate()
        {
            var startInfo = new ProcessStartInfo($@"{Basepath}\{_name}.exe");
            startInfo.Arguments = $@"{Basepath}\config_{_name}.js";

            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;

            var proc = new Process();
            proc.StartInfo = startInfo;

            proc.OutputDataReceived += (s, e) => File.AppendAllText($@"{Basepath}\{_name}_runtime.log", e.Data);
            proc.ErrorDataReceived += (s, e) => File.AppendAllText($@"{Basepath}\{_name}_eror.log", e.Data);

            proc.Start();
            proc.BeginOutputReadLine();
            proc.BeginErrorReadLine();
            proc.WaitForExit();
        }

        private byte[] GetResult()
        {
            try
            {
                var bytes = File.ReadAllBytes($@"{Basepath}\{_name}.pdf");

                return bytes;
            }
            catch (Exception e)
            {
                return new byte[0];
            }
        }

        private void CleanUp()
        {
            try
            {
                File.Delete($@"{Basepath}\{_name}.exe");
                File.Delete($@"{Basepath}\config_{_name}.js");
                File.Delete($@"{Basepath}\{_name}_runtime.log");
                File.Delete($@"{Basepath}\{_name}_eror.log");
                File.Delete($@"{Basepath}\{_name}.pdf");

                if (Type == SourceType.Local)
                    File.Delete($@"{Basepath}\source_{_name}.html");
            }
            catch (Exception e)
            {
                return;
            }
        }
    }
}