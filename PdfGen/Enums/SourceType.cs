﻿namespace PdfGen.Enums
{
    public enum SourceType
    {
        Remote = 1,
        Local = 2
    }
}