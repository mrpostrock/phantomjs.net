var page = require('webpage').create(),
    system = require('system'),
    fs = require('fs');

page.paperSize = {
    format: 'A4',
    orientation: 'portrait',
    margin: {
        left: "1cm",
        right: "1cm",
        top: "1cm",
        bottom: "1cm"
    }
};

page.settings.dpi = "96";

page.content = fs.read('*|SOURCE|*');

window.setTimeout(function () {
    page.render('*|OUTPUT|*', {format: 'pdf'});
    phantom.exit(0);
}, 500);