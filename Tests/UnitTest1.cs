﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PdfGen.Impl;
using System;
using System.IO;
using System.Net;
using System.Net.Http;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var t = new PhantomJs();
            //var html = new HttpClient()
            //    .GetAsync("https://stage.genedx.com/exgn/mail.php?id=5119")
            //    .Result
            //    .Content
            //    .ReadAsStringAsync()
            //    .Result;


            var html = File.ReadAllText($@"C:\Users\mrpos\Desktop\pdf_template.html");


            var file = t.GeneratePdf(html);

            File.WriteAllBytes($@"C:\Users\mrpos\Desktop\{Guid.NewGuid()}.pdf", file);

            Assert.IsNotNull(file);
            Assert.IsTrue(file.Length > 0);
        }
    }
}
